package po;

/**
 *
 * @author Rafael Godoi Orbolato <rafael at iftm.edu.br>
 */
public class No {
    public int valor;
    public No anterior;
    public No proximo;
    
    public No() {
        valor = 0;
        anterior = null;
        proximo = null;
    }
    
    public No(int valor) {
        this.valor = valor;
        anterior = null;
        proximo = null;
    }
        
    public No(int valor, No anterior, No proximo) {
        this.valor = valor;
        this.anterior = anterior;
        this.proximo = proximo;
    }
            
}
