package po;

/**
 *
 * @author Rafael Godoi Orbolato <rafael at iftm.edu.br>
 */
public class ListaDuplamenteLigada {

    private No inicio;
    private No fim;
    private int tamanho;

    public ListaDuplamenteLigada() {
        tamanho = 0;
        inicio = null;
        fim = null;
    }

    public void insereInicio(int valor) {
        if (tamanho == 0) {  // primeiro elemento
            inicio = new No(valor);
            fim = inicio;
        } else {
            inicio = new No(valor, null, inicio);
            inicio.proximo.anterior = inicio;
        }
        tamanho++;
    }

    public void insereFim(int valor) {
        if (tamanho == 0) {  // primeiro elemento
            inicio = new No(valor);
            fim = inicio;
        } else {
            fim = new No(valor, fim, null);
            fim.anterior.proximo = fim;
        }
        tamanho++;
    }

    public void inserePosicao(int valor, int posicao) {
        if ((posicao < 0) || (posicao > tamanho - 1)) {
            throw new IndexOutOfBoundsException();
        } else {
            if (posicao == 0) {
                insereInicio(valor);
            } else {
                No aux = inicio;
                int cont = 0;
                while (cont < posicao) {
                    aux = aux.proximo;
                    cont++;
                }
                No novo = new No(valor, aux.anterior, aux);
                aux.anterior.proximo = novo;
                aux.anterior = novo;
                tamanho++;
            }
        }
    }

    public void removeInicio() {
        if (tamanho != 0) {
            if (tamanho == 1) {
                inicio = null;
                fim = null;
            } else {
                inicio = inicio.proximo;
                inicio.anterior = null;
            }
            tamanho--;
        } else {
            throw new IndexOutOfBoundsException("Impossivel remover inicio de uma lista vazia.");
        }
    }

    public void removeFim() {
        if (tamanho != 0) {
            if (tamanho == 1) {
                inicio = null;
                fim = null;
            } else {
                fim = fim.anterior;
                fim.proximo = null;
            }
            tamanho--;
        } else {
            throw new IndexOutOfBoundsException("Impossivel remover o fim de uma lista vazia.");
        }
    }

    public int removePosicao(int posicao) {
        int retorno = 0;
        if (tamanho == 0) {
            throw new IndexOutOfBoundsException("Impossivel remover uma posicao de uma lista vazia.");
        } else if ((posicao <= 0) || (posicao > tamanho - 1)) {
            throw new IndexOutOfBoundsException("Remocao em posicao invalida.");
        } else {
            int cont = 0;
            No aux = inicio;

            while (cont < posicao) {
                aux = aux.proximo;
                cont++;
            }
            if (retorno == 0) {
                retorno = aux.valor;
            }
            aux.anterior.proximo = aux.proximo;
            aux.proximo.anterior = aux.anterior;
            tamanho--;
        }
        return retorno;
    }

    public int getValorInicio() {
        if (tamanho != 0) {
            return inicio.valor;
        } else {
            throw new IndexOutOfBoundsException("Impossivel retornar o valor do inicio de uma lista vazia.");
        }
    }

    public void setValorInicio(int valor) {
        if (tamanho != 0) {
            inicio.valor = valor;
        } else {
            throw new IndexOutOfBoundsException("Impossivel setar o valor do inicio de uma lista vazia.");
        }
    }

    public int getValorFim() {
        if (tamanho != 0) {
            return fim.valor;
        } else {
            throw new IndexOutOfBoundsException("Impossivel retornar o valor do fim de uma lista vazia.");
        }
    }

    public void setValorFim(int valor) {
        if (tamanho != 0) {
            fim.valor = valor;
        } else {
            throw new IndexOutOfBoundsException("Impossivel setar o valor do fim de uma lista vazia.");
        }
    }

    public int getValorPosicao(int posicao) {
        if (tamanho != 0) {
            if ((posicao <= 0) || (posicao > tamanho - 1)) {
                throw new IndexOutOfBoundsException("Impossivel retornar o valor de uma posicao invalida.");
            } else {
                int cont = 0;
                No aux = inicio;

                while (cont < posicao) {
                    aux = aux.proximo;
                    cont++;
                }

                return aux.valor;
            }
        } else {
            throw new IndexOutOfBoundsException("Impossivel retornar o valor de uma posicao de uma lista vazia.");
        }
    }

    public void setValorPosicao(int valor, int posicao) {
        if (tamanho != 0) {
            if ((posicao <= 0) || (posicao > tamanho - 1)) {
                throw new IndexOutOfBoundsException("Impossivel setar o valor de uma posicao invalida.");
            } else {
                int cont = 0;
                No aux = inicio;

                while (cont < posicao) {
                    aux = aux.proximo;
                    cont++;
                }

                aux.valor = valor;
            }
        } else {
            throw new IndexOutOfBoundsException("Impossivel setar o valor de uma posicao de uma lista vazia.");
        }
    }

    public boolean existeValor(int valor) {
        return getPosicaoValor(valor) != -1;
    }

    public int getPosicaoValor(int valor) {
        if (tamanho != 0) {
            int cont = 0;
            No aux = inicio;

            while ((aux != null) && (aux.valor != valor)) {
                aux = aux.proximo;
                cont++;
            }
            if (cont != tamanho) {
                return cont;
            } else {
                return -1;
            }
        } else {
            return -1;
        }
    }

    public int getTamanho() {
        return tamanho;
    }

    public void limpar() {
        No aux = inicio, prox;

        while (aux != null) {
            prox = aux.proximo;
            aux.anterior = null;
            aux.proximo = null;
            aux = prox;
        }

        inicio = null;
        fim = null;
        tamanho = 0;
    }

    public boolean isVazia() {
        return tamanho == 0;
    }

    public String toString() {
        String retorno = "";
        No aux = inicio;
        if (aux != null) {
            retorno += aux.valor;
            aux = aux.proximo;
            while (aux != null) {
                retorno += " <-> ";
                retorno += aux.valor;
                aux = aux.proximo;
            }
        }
        return retorno;
    }
    
    public String toStringInvertido() {
        String retorno = "";
        No aux = fim;
        if (aux != null) {
            retorno += aux.valor;
            aux = aux.anterior;
            while (aux != null) {
                retorno += " <-> ";
                retorno += aux.valor;
                aux = aux.anterior;
            }
        }
        return retorno;
    }

}
