/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dupla_1;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Ignore;
import po.ListaDuplamenteLigada;

/**
 *
 * @author aluno
 */
public class ListaDuplamenteLigadaTest {
    
    public ListaDuplamenteLigadaTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of insereInicio method, of class ListaDuplamenteLigada.
     */
    @Test
    public void testInsereInicio() {
        System.out.println("insereInicio");
        int valor = 4;
        ListaDuplamenteLigada instance = new ListaDuplamenteLigada();
        instance.insereInicio(valor);
      }
    
    @Test
    public void testInsereInicioTamanhoMaiorQueZero() {
        System.out.println("insereInicio");
        int v1 = 4;
        ListaDuplamenteLigada list = new ListaDuplamenteLigada();
        list.insereInicio(v1);
        v1 = 5;
        list.insereInicio(v1);
        
      }

    /**
     * Test of removePosicao method, of class ListaDuplamenteLigada.
     */
    @Test(expected = IndexOutOfBoundsException.class) 
    public void testRemovePosicao() {
        System.out.println("removePosicao");
        int posicao = 2;
        ListaDuplamenteLigada instance = new ListaDuplamenteLigada();
        int expResult = 0;
        int result = instance.removePosicao(posicao);
        
        assertEquals(expResult, result);
    }
    
    /**
     * Test of removePosicao method, of class ListaDuplamenteLigada.
     */
    @Test(expected = IndexOutOfBoundsException.class) 
    public void testRemovePosicaoInvalida() {
        System.out.println("removePosicao");
        int posicao = 2;
        ListaDuplamenteLigada instance = new ListaDuplamenteLigada();
        instance.insereInicio(1);
        int expResult = 0;
        int result = instance.removePosicao(posicao);
        
        assertEquals(expResult, result);
    }
    
    @Ignore
    @Test
    public void testRemovePosicaoSuccess() {
        System.out.println("removePosicao");
        int posicao = 1;
        ListaDuplamenteLigada instance = new ListaDuplamenteLigada();
        instance.insereInicio(4);
        instance.insereInicio(5);
        int expResult = 4;
        int result = instance.removePosicao(posicao);
        
        assertEquals(expResult, result);
    }

    /**
     * Test of getValorInicio method, of class ListaDuplamenteLigada.
     */
    @Test
    public void testGetValorInicio() {
        System.out.println("getValorInicio");
        ListaDuplamenteLigada instance = new ListaDuplamenteLigada();
        instance.insereInicio(4);
        int expResult = 4;
        int result = instance.getValorInicio();
        
        assertEquals(expResult, result);
    }
    
    /**
     * Test of getValorInicio method, of class ListaDuplamenteLigada.
     */
    @Test(expected = IndexOutOfBoundsException.class)
    public void testGetValorInicioInvalid() {
        System.out.println("getValorInicio");
        ListaDuplamenteLigada instance = new ListaDuplamenteLigada();
        int expResult = 1;
        int result = instance.getValorInicio();
        
        assertEquals(expResult, result);
    }

    /**
     * Test of setValorPosicao method, of class ListaDuplamenteLigada.
     */
    @Test(expected = IndexOutOfBoundsException.class)
    public void testSetValorPosicaoTamanhoZero() {
        System.out.println("setValorPosicao");
        int valor = 0;
        int posicao = 0;
        ListaDuplamenteLigada instance = new ListaDuplamenteLigada();
        instance.setValorPosicao(valor, posicao);
    }
    
    /**
     * Test of setValorPosicao method, of class ListaDuplamenteLigada.
     */
    @Test(expected = IndexOutOfBoundsException.class)
    public void testSetValorPosicaoInvalida() {
        System.out.println("setValorPosicao");
        int valor = 3;
        int posicao = 5;
        ListaDuplamenteLigada instance = new ListaDuplamenteLigada();
        instance.insereInicio(2);
        instance.setValorPosicao(valor, posicao);
    }    

    /**
     * Test of setValorPosicao method, of class ListaDuplamenteLigada.
     */
    @Test
    public void testSetValorPosicaoSuccess() {
        System.out.println("setValorPosicao");
        int valor = 4;
        int posicao = 2;
        ListaDuplamenteLigada list = new ListaDuplamenteLigada();
        
        list.insereInicio(1);
        list.insereInicio(2);
        list.insereInicio(3);
        
        list.setValorPosicao(valor, posicao);
    }  
    
    /**
     * Test of existeValor method, of class ListaDuplamenteLigada.
     */
    @Test
    public void testExisteValorSuccess() {
        System.out.println("Test the method existe valor with success");
        int valor = 2;
        ListaDuplamenteLigada list = new ListaDuplamenteLigada();
        list.insereInicio(2);
        boolean expResult = true;
        boolean result = list.existeValor(valor);
        
        assertEquals(expResult, result);
    }
    
     /**
     * Test of existeValor method, of class ListaDuplamenteLigada.
     */
    @Test
    public void testExisteValorNotFound() {
        System.out.println("Test the method existe valor with parameter not found");
        int valor = 0;
        ListaDuplamenteLigada instance = new ListaDuplamenteLigada();
        boolean expResult = false;
        boolean result = instance.existeValor(valor);
        
        assertEquals(expResult, result);
    }

    /**
     * Test of isVazia method, of class ListaDuplamenteLigada.
     */
    @Test
    public void testIsVaziaInvalid() {
        System.out.println("isVazia");
        ListaDuplamenteLigada instance = new ListaDuplamenteLigada();
        instance.insereInicio(1);
        boolean expResult = false;
        boolean result = instance.isVazia();
        assertEquals(expResult, result);
    }
   
    /**
     * Test of isVazia method, of class ListaDuplamenteLigada.
     */
    @Test
    public void testIsVaziaSuccess() {
        System.out.println("isVazia");
        ListaDuplamenteLigada instance = new ListaDuplamenteLigada();
        boolean expResult = true;
        boolean result = instance.isVazia();
        assertEquals(expResult, result);
    }
}
